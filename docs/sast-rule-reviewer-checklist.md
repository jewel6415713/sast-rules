# SAST-RULE Reviewer Checklist

#### MRE Review

This is for the applications under: [sast-rules-apps](https://gitlab.com/gitlab-org/security-products/tests/sast-rules-apps)

- [ ] Checkout the feature branch
- [ ] Insure there are build steps if not standard addition to servlets
- [ ] Insure there are 'testing' steps, e.g. URLs to load to test in comments or a README so the check can be validated
- [ ] If a framework, read the framework documentation to understand the vulnerability/risk
- [ ] Find sources (web links/papers) for exploitation scenarios or steps for the MRE.
- [ ] Load the URL or host/port into Burp Community or curl to test the endpoint
- [ ] If an attack requires 'special characters', make sure the test has a post methods to actually confirm that the web server isn't filtering out invalid URLs
- [ ] When testing, try both positive and negative cases, in particular try really hard to make the negative cases fail (e.g. show it can actually be exploitable when the author assumes it isn't). In the past we've identified at least 3 cases where the author had misconceptions about how sanitizing or certain protections work. See examples:
	- [ ] https://gitlab.com/gitlab-org/security-products/tests/sast-rules-apps/java-web-apps/-/merge_requests/41#note_1794520587
	- [ ] https://gitlab.com/gitlab-org/security-products/tests/sast-rules-apps/csharp-web-apps/-/merge_requests/2#note_1720722069
	- [ ] https://gitlab.com/gitlab-org/security-products/tests/sast-rules-apps/java-web-apps/-/merge_requests/23#note_1765392454
- [ ] Update any tests and comment back to author if the author could not get something you could get working/exploit
- [ ] Ensure variants of a flaw are accounted for

### sast-rule review checklist (updating)

This is for rules being enhanced or updated under [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/).

- [ ] Ensure the test case has both positive `// ruleid: ...` and negative `// ok: ....` cases.
- [ ] If pattern is not a taint mode pattern, ensure `pattern-not` variations exist to disregard constant strings e.g.: `pattern-not: some_sink("...")`
- [ ] If pattern is taint mode, as much as possible make sure they offer some sanitizer pattern otherwise it will be hard for customers to actually fix the flaws from showing up.
	- [ ] Review other taint mode rules for the particular language to see if the sources should be shared. (We hopefully can standardize this in the future)
- [ ] You have to be really diligent in thinking about how a pattern could FP, ensure patterns use fully qualified names or types for imports, prefer: `($X.servlet.http.HttpServletResponse $RESP).addCookie($C);` over `$RESPONSE.addCookie($C)`
- [ ] Ensure the following metadata fields exist:
	- [ ] `owasp:` with both 2017 and 2021 mappings, see https://owasp.org/www-project-top-ten/assets/images/mapping.png for how they map to each other.
	- [ ] `category: security` this is important for various metadata processing
	- [ ] `cwe` this should only have the cwe number prefixed by CWE-, not text, e.g.: `cwe: CWE-76` and not `cwe: CWE-76 Some Other Text`
	- [ ] `shortDescription` This is the "title" of the vulnerability as it shows up in the dashboard, make sure it follows the technical writing guidelines of only the first word's character being capitalized. e.g:  use `"Use of a broken or risky cryptographic algorithm` *NOT* `"Use of a Broken or Risky Cryptographic Algorithm`
	- [ ] `security-severity` this metadata field will take over for the `severity`, but both are required for now. Note in the future we will apply more applicable security-severity levels but the general mapping is:
		- [ ] severity INFO == security-severity LOW
		- [ ] severity WARNING == security-severity MEDIUM
		- [ ] severity ERROR == security-severity CRITICAL
- [ ] Run BAP to verify the changes do not cause wild fluctuations in findings or cause other issues.

### sast-rule review checklist (new rule)

This is for new rules being added under [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/).

- [ ] Do all of the above regarding updating a rule
- [ ] Ensure the description text is valid and contains a secure code example, remove any example code that shows bad / vulnerable usage. Prefer looking at other languages protections first before custom new code / protection ideas are created
- [ ] Make sure the mapping is correct if the rule fits / can be mapped to an "original analyzer". 
- [ ] If it's a brand new rule:
	- [ ] Did the author reference/use code from community rules? If so, apply the applicable label and put it in the applicable licensed directory (lgpl, lgpl-cc etc).
	- [ ] If it's a fully custom rule it needs to have the GitLab EE license applied.
	- [ ] Add the mapping to the `gitlab_<language>.yml`