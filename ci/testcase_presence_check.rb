#!/usr/bin/env ruby

# TestCasePrecenceCheck ensures that a test case file with expected name and
# file extension exists for all rules.
module TestCasePresenceCheck
  def self.run
    ok = true
    Dir.glob('./**/*.yml').each do |file|
      next if ['.git', '..', '.', 'ci', 'dist', 'docs', 'mappings', 'qa'].include?(file.split('/')[1])

      lang = self.lang_from_path(file)
      exts = self.extensions_for_lang(lang)

      test_files = []

      dirname = File.dirname(file)
      basename = File.basename(file, ".yml").delete_prefix("rule-")

      exts.each do |ext|
        test_files.push("#{dirname}/rule-#{basename}.#{ext}")
      end

      test_file_found = ""

      test_files.each do |test_file|
        if File.file?(test_file)
          test_file_found = test_file

          break
        end
      end

      if test_file_found.empty?
        puts("no test case file for #{file}: ✘")
        ok = false

        next
      end

      if self.empty_test_file?(test_file_found)
        puts("empty test case file #{test_file_found}: ✘")
        # TODO: When all test case files contain example code, this check should
        # be activated to fail the build when the test file is empty.
        # ok = false
      end

    end
    ok
  end

  def self.lang_from_path(path)
    if path.split('/')[1] == 'rules'
      path = File.join('./', path.split('/')[3..])
    end

    return path.split('/')[1]
  end

  def self.extensions_for_lang(lang)
    case lang
    when 'csharp'
      return ['cs']
    when 'javascript'
      return ['js', 'html']
    when 'python'
      return ['py']
    when 'ruby'
      return ['rb']
    else
      return [lang]
    end
  end

  def self.empty_test_file?(filename)
    File.open(filename, "r") do |f|
      code_lines = f.readlines.delete_if { |l| l.strip!; l == "" || l.start_with?("//") }

      return code_lines.empty?
    end
  end
end

if TestCasePresenceCheck.run
  puts("test case files exist for all rules: ✔")
  exit(0)
else
  exit(-1)
end
