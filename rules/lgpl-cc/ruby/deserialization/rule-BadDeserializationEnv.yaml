# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_deserialization_rule-BadDeserializationEnv"
  mode: taint
  pattern-sources:
  - pattern-either:
    - pattern: request.env
  pattern-sinks:
  - pattern-either:
    - pattern: |
        Marshal.load(...)
    - pattern: |
        Marshal.restore(...)
    - pattern: |
        Oj.object_load(...)
    - pattern: |
        Oj.load($X)
  message: |
    In Ruby, objects can be serialized into strings using various methods 
    and later reconstituted into objects. The `load` and `object_load` 
    methods, associated with modules like Marshal and CSV, is particularly 
    risky when used to deserialize data from untrusted sources. 
    If an attacker is able to manipulate the serialized data, they could 
    execute arbitrary code on the system when the data is deserialized. 
    This vulnerability can lead to remote code execution (RCE), where an 
    attacker gains the ability to execute commands on the host machine. 
    The Marshal and CSV modules are often used for serialization and 
    deserialization in Ruby but they do not inherently sanitize or validate 
    the data being processed. This makes them unsafe for handling input from 
    untrusted sources.

    To mitigate the risks associated with unsafe deserialization, it is 
    recommended to:
    - Avoid deserializing from untrusted sources
    - Use JSON for serialization/deserialization as JSON is considered 
    safer for data interchange between systems. However, it's crucial 
    to use JSON securely by validating and sanitizing the input before 
    deserialization.

    Secure Code Example:
    ```
    require 'json'

    def safe_deserialize(json_data)
      # Validate and sanitize json_data before parsing
      begin
        parsed_data = JSON.parse(json_data)
        return parsed_data
      rescue JSON::ParserError => e
        puts "Error parsing JSON data: #{e.message}"
        return nil
      end
    end

    # Example usage
    # Example of input JSON string
    json_input = '{"name": "John Doe", "age": 30}' 
    user_data = safe_deserialize(json_input)
    puts user_data.inspect if user_data
    ```

    Always ensure that any data deserialized from JSON (or any format) is 
    validated and sanitized according to the context of your application 
    to prevent injection attacks or other security issues.
  languages:
  - "ruby"
  severity: "ERROR"
  metadata:
    shortDescription: "Deserialization of untrusted data"
    category: "security"
    cwe: "CWE-502"
    owasp:
    - "A8:2017-Insecure Deserialization"
    - "A08:2021-Software and Data Integrity Failures"
    security-severity: "HIGH"
    technology:
    - "ruby"
    references:
    - "https://groups.google.com/g/rubyonrails-security/c/61bkgvnSGTQ/m/nehwjA8tQ8EJ"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/bad-deserialization-env.yaml"