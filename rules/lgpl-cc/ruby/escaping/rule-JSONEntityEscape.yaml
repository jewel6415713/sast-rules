# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_escaping_rule-JSONEntityEscape"
  pattern-either:
  - pattern: |
      ActiveSupport.escape_html_entities_in_json = false
  - pattern: |
      config.active_support.escape_html_entities_in_json = false
  message: |
    This rule checks if HTML escaping is globally disabled for JSON output, 
    which can lead to Cross-Site Scripting (XSS) vulnerabilities. 
    XSS attacks allow attackers to inject malicious scripts into web pages 
    viewed by other users, compromising the integrity and confidentiality 
    of user data. When HTML escaping is disabled, special HTML characters 
    in JSON output are not converted to their entity equivalents, making 
    it possible for an attacker to inject executable scripts into the web 
    application's output. 
    
    To mitigate this risk, ensure that HTML escaping is enabled when 
    generating JSON output, particularly in web applications 
    that dynamically insert user-generated content into the DOM.

    Secure Code Example:
    ```
    # For an initializer configuration (e.g., in config/initializers/active_support.rb)
    # Enable HTML entity escaping in JSON to prevent XSS
    ActiveSupport.escape_html_entities_in_json = true

    # For application-wide configuration (e.g., in config/application.rb)
    module YourApplication
      class Application < Rails::Application
        # Enable HTML entity escaping in JSON to prevent XSS
        config.active_support.escape_html_entities_in_json = true
      end
    end
    ```
  languages:
  - "ruby"
  metadata:
    shortDescription: "Improper neutralization of input during web page generation
      ('Cross-site Scripting')"
    category: "security"
    cwe: "CWE-79"
    owasp:
    - "A7:2017-Cross-Site Scripting (XSS)"
    - "A03:2021-Injection"
    security-severity: "MEDIUM"
    technology:
    - "rails"
    references:
    - "https://owasp.org/Top10/A03_2021-Injection"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/json-entity-escape.yaml"
  severity: "WARNING"