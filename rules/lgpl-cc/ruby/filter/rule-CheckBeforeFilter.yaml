# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_filter_rule-CheckBeforeFilter"
  mode: search
  patterns:
  - pattern-either:
    - pattern: |
        skip_filter ..., :except => $ARGS
    - pattern: |
        skip_before_filter ..., :except => $ARGS
  message: |
    The application was found disabling controller checks by setting `skip_filter` 
    or `skip_before_filter` with an `:except` option. This approach can 
    inadvertently open up parts of your application to unauthorized access 
    because it relies on a blocklist approach, where only specified actions 
    are protected. 
    
    A safer method of providing this functionality involves specifying 
    exactly which controller actions should have checks applied using an 
    `:only` option, effectively creating an allowlist. This method ensures 
    that only specified actions are affected, and any new actions added to 
    the controller will have the filters applied by default, adhering to the 
    principle of secure by default.

    Secure Code Example:
    ```
    class UsersController < ApplicationController
      # Apply the filter only to these actions, making it clear and secure by default
      skip_before_action :authenticate_user!, only: [:new, :create]
    end
    ```

    In the secure example, `:authenticate_user!` filter is explicitly skipped 
    only for the `:new` and `:create actions`. This means any new action added 
    to the UsersController in the future will have the `authenticate_user!` 
    filter applied by default, ensuring that new parts of the application 
    are secure from the start. 
  languages:
  - "ruby"
  severity: "WARNING"
  metadata:
    owasp:
    - "A5:2017-Broken Access Control"
    - "A01:2021-Broken Access Control"
    cwe: "CWE-284"
    shortDescription: "Improper access control"    
    references:
    - "https://owasp.org/Top10/A01_2021-Broken_Access_Control/"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/brakeman/check-before-filter.yaml"
    category: "security"
    security-severity: "MEDIUM"
    technology:
    - "ruby"
    - "rails"