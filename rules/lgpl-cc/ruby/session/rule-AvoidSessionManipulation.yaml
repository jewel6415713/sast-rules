# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_session_rule-AvoidSessionManipulation"
  mode: taint
  pattern-sources:
  - pattern: params
  - pattern: cookies
  - pattern: request.env
  pattern-sinks:
  - pattern: session[...]
  message: |
    The application was found retrieving session data using user input. A 
    malicious user may be able to retrieve information from the session 
    that was not meant to be allowed. 
    Session manipulation can occur when an application allows user-input in 
    session keys. Since sessions are typically considered a source of truth 
    (e.g. to check the logged-in user or to match CSRF tokens), allowing an 
    attacker to manipulate the session may lead to unintended behavior. 
    
    To mitigate this issue, never use user input as a session key. Instead, 
    consider an allow list approach to control access to session keys, 
    ensuring only predefined keys are accessible, and user input is not used
    to directly access the session key values. 

    Secure Code Example:
    ```
    # Define an allowed list of permitted session keys
    ALLOWED_SESSION_KEYS = ['display_settings', 'locale']
    
    user_provided_key = params[:key]

    # Validate the key against the list
    if ALLOWED_SESSION_KEYS.include?(user_provided_key)
      # Access the session value safely
      value = session[user_provided_key]
    else
      raise "Invalid session key provided."
    end
    ```
  languages: 
  - "ruby"
  severity: "ERROR"
  metadata:
    shortDescription: "Incorrect default permissions"
    owasp:
    - "A5:2017-Broken Access Control"
    - "A01:2021-Broken Access Control"
    cwe: "CWE-276"
    security-severity: "CRITICAL"
    references:
    - "https://brakemanscanner.org/docs/warning_types/session_manipulation/"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/audit/avoid-session-manipulation.yaml"
    category: "security"
    technology:
    - "rails"
  